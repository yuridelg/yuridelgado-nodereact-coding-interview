import axios from "axios";

import { IUserProps } from "../dtos/user.dto";

interface ClientOptions {
  pagination?: {
    page: number;
  };
  search?: {
    title: string;
  };
}

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(options?: ClientOptions): Promise<{ data: IUserProps[] }> {
    return (
      await axios.get(`${this.baseUrl}/people`, {
        params: {
          page: options?.pagination?.page || 0,
          title: options?.search?.title,
        },
      })
    ).data;
  }
}
