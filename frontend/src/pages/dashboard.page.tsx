import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { uniqBy } from "lodash";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const [titleSearch, setTitleSearch] = useState("");
  const [page, setPage] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await backendClient.getAllUsers({
          pagination: { page },
          search: {
            title: titleSearch,
          },
        });
        setUsers((prev) => uniqBy([...prev, ...result.data], (el) => el.id));
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [page, titleSearch]);

  useEffect(() => {
    setPage(0);
    setUsers([]);
  }, [titleSearch]);

  const handleLoadMore = () => {
    setPage((prev) => prev + 1);
  };

  const handleSearchTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.target.value;
    setTitleSearch(val);
  };

  return (
    <div style={{ paddingTop: "30px" }}>
      <div>
        <form>
          <input
            placeholder="Title"
            type="text"
            name="title"
            onChange={handleSearchTitleChange}
            value={titleSearch}
          />
          <button>Search</button>
        </form>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>

      <button onClick={handleLoadMore}>Load more</button>
    </div>
  );
};
