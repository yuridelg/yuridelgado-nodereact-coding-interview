import people_data from "../data/people_data.json";

interface SearchOptions {
  title?: string;
}
export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(searchOptions?: SearchOptions) {
    const { title } = searchOptions || {};

    if (title) {
      return people_data.filter((person) =>
        person.title?.toLowerCase()?.includes(title.toLowerCase())
      );
    }

    return people_data;
  }
}
