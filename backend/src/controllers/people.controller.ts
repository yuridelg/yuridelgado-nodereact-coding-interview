import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParam,
} from "routing-controllers";

import { PeopleProcessing } from "../services/people_processing.service";

const peopleProcessing = new PeopleProcessing();

const PAGE_SIZE = 10;

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  // v1/people/?title=&gender=&company=
  @HttpCode(200)
  @Get("/")
  getAllPeople(
    @QueryParam("page") page?: number,
    @QueryParam("title") title?: string
  ) {
    const people = peopleProcessing.getAll({ title });

    if (!people) {
      throw new NotFoundError("No people found");
    }

    const offset = (page || 0) * PAGE_SIZE;

    return {
      data: people.slice(offset, offset + PAGE_SIZE),
    };
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }
}
